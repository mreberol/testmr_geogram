WARNING: this is a temporary repository for testing, do not use it for development.

# Geogram

Geogram is a programming library of geometric algorithms. It includes a simple
yet efficient Mesh data structure (for surfacic and volumetric meshes), exact
computer arithmetics (a-la Shewchuck, implemented in GEO::expansion), a
predicate code generator (PCK: Predicate Construction Kit), standard geometric
predicates (orient/insphere), Delaunay triangulation, Voronoi diagram, spatial
search data structures, spatial sorting) and less standard ones (more general
geometric predicates, intersection between a Voronoi diagram and a triangular
or tetrahedral mesh embedded in n dimensions). 
